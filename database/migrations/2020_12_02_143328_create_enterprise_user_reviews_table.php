<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnterpriseUserReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enterprise_user_reviews', function (Blueprint $table) {
            $table->id();
            $table->integer("enterprise_id")->unsigned();
            $table->foreign("enterprise_id")->references("id")->on("enterprises")->onDelete("cascade");
            $table->integer("user_id")->unsigned();
            $table->foreign("user_id")->references("id")->on("users")->onDelete("cascade");
            $table->integer("type_id")->unsigned();
            $table->foreign("type_id")->references("id")->on("enterprise_review_types")->onDelete("cascade");
            $table->integer("review", 0, 5);
            $table->timestamps();
            $table->unique(["enterprise_id", "user_id", "type_id"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enterprise_user_reviews');
    }
}
