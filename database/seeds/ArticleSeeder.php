<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Article;
use Illuminate\Support\Facades\DB;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('articles')->insert([
            'title' => "Fruits et légumes de saison : que manger en août ?",
            'img_path' => "https://fac.img.pmdstatic.net/fit/http.3A.2F.2Fprd2-bone-image.2Es3-website-eu-west-1.2Eamazonaws.2Ecom.2FFAC.2Fvar.2Ffemmeactuelle.2Fstorage.2Fimages.2Fcuisine.2Fguides-cuisine.2Fcuisiner-de-saison-manger-en-aout-42082.2F14647161-1-fre-FR.2Fcuisiner-de-saison-que-manger-en-aout.2Ejpg/850x478/quality/90/crop-from/center/fruits-et-legumes-de-saison-que-manger-en-aout.jpeg",
            'date' => "2020-06-11",
            'description' => "Avec ou sans canicule, les agriculteurs et agricultrices de nos régions parviennent à réaliser des miracles chaque année en Août : une avalanche de produits frais déferle sur les marchés, tous plus colorés et savoureux les uns que les autres.",
            'alt' => "fruits & légumes",
            'link' => "https://www.femmeactuelle.fr/cuisine/guides-cuisine/cuisiner-de-saison-manger-en-aout-42082",
        ]);
        DB::table('articles')->insert([
            'title' => "Circuits courts et productions locales, notre sélection d'adresses pour être locavore à Toulouse",
            'img_path' => "https://static.actu.fr/uploads/2018/10/25564-181012175251904-0-960x640.jpg",
            'date' => "2020-07-23",
            'description' => "Privilégier les circuits courts pour limiter son empreinte carbone ou favoriser l'économie locale : à Toulouse, il y a de nombreuses possibilités pour consommer local. Florilège.",
            'alt' => "Circuit toulouse",
            'link' => "https://actu.fr/occitanie/toulouse_31555/circuits-courts-productions-locales-selection-dadresses-etre-locavore-toulouse_19047220.html",
        ]);
        DB::table('articles')->insert([
            'title' => "Comment mieux consommer sans se ruiner",
            'img_path' => "https://i.f1g.fr/media/figaro/616x347_cropupscale/2016/06/18/XVM8d373578-34ad-11e6-b0d9-b6a14f932977.jpg",
            'date' => "2020-06-16",
            'description' => "Pour aider les petits producteurs, j'ai décidé de me passer des supermarchés. L'expérience collaborative m'a permis de tester de nouvelles adresses, astuces et recettes.",
            'alt' => "Marchés",
            'link' => "https://www.lefigaro.fr/conso/2016/06/18/05007-20160618ARTFIG00009-comment-mieux-consommer-sans-se-ruiner8230petit-guide-a-l-usage-des-non-inities.php",
        ]);
        DB::table('articles')->insert([
            'title' => "Les 5 tables locavores à Paris",
            'img_path' => "https://i.f1g.fr/media/figarofr/616x347_cropupscale/2014/09/11/PHOf751b8ea-399e-11e4-82e9-981a21cf099a-805x453.jpg",
            'date' => "2020-06-14",
            'description' => "l'occasion des Semaines du Manger Local, du 13 au 28 septembre, voici cinq bons restos parisiens qui mettent en avant les petits producteurs de la région Ile-de-France.",
            'alt' => "Plat",
            'link' => "https://www.lefigaro.fr/sortir-paris/2014/09/11/30004-20140911ARTFIG00328-les-5-tables-locavores-a-paris.php",
        ]);
        DB::table('articles')->insert([
            'title' => "La crise sanitaire Une opportunité pour le MIN ?",
            'img_path' => "https://images.petitbleu.fr/api/v1/images/view/5fbddad63e454671a77fe876/large/image.jpg?v=1",
            'date' => "2020-07-13",
            'description' => "Le Marché d’Intérêt National de Toulouse a su tirer son épingle du jeu malgré la crise sanitaire. Illustration avec l’entreprise Mon panier de Campagne, hébergée sur le site du MIN et qui est en pleine croissance.",
            'alt' => "le MIN",
            'link' => "https://www.petitbleu.fr/2020/11/25/la-crise-sanitaire-une-opportunite-pour-le-min-9219550.php",
        ]);
        DB::table('articles')->insert([
            'title' => "Aude : Stéphane, locavore de Castelnaudary, milite pour les produits locaux et la résilience alimentaire",
            'img_path' => "https://france3-regions.francetvinfo.fr/image/ScbHSHo4SSYgHPazd7S6BjKPsaI/930x620/regions/2020/06/09/5edfa1c33902d_s_portrait_stephane_linou_militant_du_local_-00_01_53_07-4585285.jpg",
            'date' => "2020-01-03",
            'description' => "Rencontre avec Stéphane Linou, il est audois et c'est l’un des premiers locavores de France. Cela signifie qu’il consomme des aliments produits localement. Au delà de l’aspect environnemental, ce Chaurien en a fait une idée politique : la résilience alimentaire.",
            'alt' => "Stephane",
            'link' => "https://france3-regions.francetvinfo.fr/occitanie/aude/aude-stephane-locavore-castelnaudary-milite-produits-locaux-resilience-alimentaire-1769349.html",
        ]);
        DB::table('articles')->insert([
            'title' => "J'irai manger local chez vous : une ferme Lotoise relève le défi",
            'img_path' => "https://images.ladepeche.fr/api/v1/images/view/5f6379cd8fe56f3e1b28fa58/large/image.jpg?v=1",
            'date' => "2020-09-17",
            'description' => "Benjamin Zimra, de la ferme de Vanadal, et Claire Mauquié, spécialiste en autonomie alimentaire, ont relevé le défi locavore lancé par Stéphane Linou : préparer un repas de fête, avec des produits issus d'un rayon de 51 km et pour moins de 9,50€.",
            'alt' => "Ferme",
            'link' => "https://www.ladepeche.fr/2020/09/17/un-repas-local-chic-et-pas-cher-a-escamps-la-ferme-de-vanadal-releve-le-defi-locavore-9077526.php",
        ]);
        DB::table('articles')->insert([
            'title' => "Le locavore Stéphane Linou appelle à transformer les stades de foot et rugby en champs de patates",
            'img_path' => "https://static.actu.fr/uploads/2018/08/25525-180823103227671-0-960x640.jpg",
            'date' => "2020-09-05",
            'description' => "Le locavore Stéphane Linou appelle les élus à transformer leurs espaces verts en jardins nourriciers pour produire des ressources alimentaires à distribuer aux populations.",
            'alt' => "Stephane2",
            'link' => "https://actu.fr/occitanie/castelnaudary_11076/le-locavore-stephane-linou-appelle-transformer-stades-foot-rugby-champs-patates_33479347.html",
        ]);
        DB::table('articles')->insert([
            'title' => "Relocaliser l’alimentation : une question de sécurité ? », conférence à Toulouse (Haute-Garonne)",
            'img_path' => "https://reporterre.net/local/cache-vignettes/L646xH500/arton19593-ef990.png?1597430532",
            'date' => "2020-09-02",
            'description' => "Manger local, c’est bon pour le goût, la santé, les paysans, l’environnement,... mais si c’était aussi une question de sécurité civile ? Comment accroître notre résilience alors que les magasins n’ont que trois jours de stocks ?",
            'alt' => "Relocaliser",
            'link' => "https://reporterre.net/Relocaliser-l-alimentation-une-question-de-securite",
        ]);
        DB::table('articles')->insert([
            'title' => "Haute-Garonne : Quand un restaurant propose à ses clients d’adopter des poules sauvées de l’abattage",
            'img_path' => "https://img.20mn.fr/z5-mgz9fQyKZPxSNE732vg/640x410_poule-picore-illustration.jpg",
            'date' => "2020-01-28",
            'description' => "Un resto de Colomiers, près de Toulouse, propose à ses clients d'adopter gratuitement des poules pondeuses sauvées de l'abattage",
            'alt' => "Poule",
            'link' => "https://www.20minutes.fr/insolite/2705311-20200128-haute-garonne-quand-restaurant-propose-clients-adopter-poules-sauvees-abattage",
        ]);
        DB::table('articles')->insert([
            'title' => "Coronavirus. À Toulouse, le restaurant le J'Go propose des paniers gourmands '100% locavore'",
            'img_path' => "https://static.actu.fr/uploads/2020/03/jgo-1-854x641.jpg",
            'date' => "2020-03-28",
            'description' => "Fermé jusqu'à nouvel ordre en raison du coronavirus, le restaurant le J'Go propose une nouvelle formule aux Toulousains qui le souhaitent : des paniers gourmands 100% locavore.",
            'alt' => "Poule",
            'link' => "https://actu.fr/occitanie/toulouse_31555/coronavirus-toulouse-restaurant-jgo-propose-paniers-gourmands-100-locavore_32541878.html",
        ]);
        DB::table('articles')->insert([
            'title' => "Millau : Naturae Bioty a le virus du bio dans la peau !",
            'img_path' => "https://images.centrepresseaveyron.fr/api/v1/images/view/5fa5575fd286c2174b3920b6/large/image.jpg?v=1",
            'date' => "2020-11-12",
            'description' => "Elodie Corocher, laborantine diplômée, spécialisée en biologie et biotechnologie, a voulu exporter les principes d’une alimentation saine et locavore dans les trousses de beauté. RDS",
            'alt' => "Poule",
            'link' => "https://www.centrepresseaveyron.fr/2020/11/06/millau-naturae-bioty-a-le-virus-du-bio-dans-la-peau-9186274.php",
        ]);
        DB::table('articles')->insert([
            'title' => "EELV lance un défi « locavore » aux montpelliérains",
            'img_path' => "https://i2.wp.com/lemouvement.info/wp-content/uploads/2020/05/EELV-Montpellier-en-campagne.jpg?resize=696%2C550&ssl=1",
            'date' => "2020-05-21",
            'description' => "Alors, à partir du 22 mai, les militants du groupe local EELV de Montpellier, lancent un défi : « pendant quinze jours, nourrissons-nous le plus possible avec des aliments produits à moins de 100 km puisque c’est le rayon de liberté qui nous est donné ».",
            'alt' => "Poule",
            'link' => "https://lemouvement.info/2020/05/21/eelv-lance-un-defi-locavore-aux-montpellierains/",
        ]);
        DB::table('articles')->insert([
            'title' => "Les locavores ne sont pas des mangeurs de locataires",
            'img_path' => "https://france3-regions.francetvinfo.fr/image/KG6aOO3MTEG-X0wS-pFm4i4KJOc/930x620/regions/2020/06/09/5edf9bb59c135_maxstockworld403024-4751284.jpg",
            'date' => "2020-05-27",
            'description' => "Résilience alimentaire, locavore, circuit court : avec la crise du Covid-19, ces mots s'affichent régulièrement dans les médias. Mais pour vous, cela reste du charabia ? Nous avons trouvé le spécialiste. Nous lui avons posé cinq questions et imposé une contrainte : des réponses simples.",
            'alt' => "Panier",
            'link' => "https://france3-regions.francetvinfo.fr/locavores-ne-sont-pas-mangeurs-locataires-resilience-alimentaire-expliquee-tous-1833986.html",
        ]);
    }
}
