<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\EnterpriseReviewType;

class EnterpriseReviewTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = collect([
            "Livraison",
            "Diversité",
            "Respect de la saisonnalité",
            "Transparence",
            "Disponibilité",
            "Fraicheur produit"
        ]);

        foreach($types as &$type) {
            if(!EnterpriseReviewType::where("name", $type)->exists()) {
                EnterpriseReviewType::create([
                    'name' => $type
                ]);
            }
        }
    }
}
