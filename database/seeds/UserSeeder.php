<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('users')->insert([
            'firstname' => 'admin',
            'lastname' => 'admin',
            'userName' => 'admin',
            'email' => 'admin@gmail.com',
            'email_verified_at' => now(),
            'password' => bcrypt('admin'), // password
            'remember_token' => 'random',
            'address' => '189 route Revel',
            'type' => "admin",
            'email_notification' => true,
            'birthDate' => now(),
        ]);

        User::factory()
                ->times(100)
                ->create();
    }
}
