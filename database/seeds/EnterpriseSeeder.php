<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Enterprise;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class EnterpriseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('enterprises')->insert([
            'name' => "ARTISAN PRIMEUR",
            'description' => "",
            'type' => "grocery",
            'city' => "Toulouse",
            'address' => "9 Rue Banières, 31100 Toulouse",
            'latitude' => 43.56275,
            'longitude' => 1.408799,
            'owner_id' => User::all()->where('type', 'professional')->random()->id,
            'like' => rand(0, 1),
        ]);

        DB::table('enterprises')->insert([
            'name' => "FROMAGERIE CRÉMERIE MOLINIE PIERRE",
            'description' => "",
            'type' => "grocery",
            'city' => "Toulouse",
            'address' => "Avenue De L'urss 31400 Toulouse",
            'latitude' => 43.581799,
            'longitude' => 1.448818,
            'owner_id' => User::all()->where('type', 'professional')->random()->id,
            'like' => rand(0, 1),
        ]);

        DB::table('enterprises')->insert([
            'name' => "BEELY",
            'description' => "",
            'type' => "grocery",
            'city' => "Villeneuve-tolosane",
            'address' => "24 Impasse Du Bourrouil, 31270 Villeneuve-tolosane",
            'latitude' => 43.524717,
            'longitude' => 1.361765,
            'owner_id' => User::all()->where('type', 'professional')->random()->id,
            'like' => rand(0, 1),
        ]);

        DB::table('enterprises')->insert([
            'name' => "LA RUCHE QUI DIT OUI - LE FAUGA",
            'description' => "",
            'type' => "grocery",
            'city' => "Le Fauga",
            'address' => "20 Chemin Roucade 31410 Le Fauga",
            'latitude' => 43.399965,
            'longitude' => 1.2913,
            'owner_id' => User::all()->where('type', 'professional')->random()->id,
            'like' => rand(0, 1),
        ]);

        DB::table('enterprises')->insert([
            'name' => "LILLY VADROUILLE",
            'description' => "",
            'type' => "grocery",
            'city' => "Saint-élix-le-château",
            'address' => "31430 Saint-élix-le-château",
            'latitude' => 43.284208,
            'longitude' => 1.149103,
            'owner_id' => User::all()->where('type', 'professional')->random()->id,
            'like' => rand(0, 1),
        ]);

        DB::table('enterprises')->insert([
            'name' => "PATISSERIE CHOCOLATERIE GALY",
            'description' => "",
            'type' => "grocery",
            'city' => "Cazères",
            'address' => "5 Rue De La Case 31220 Cazères",
            'latitude' => 43.206688,
            'longitude' => 1.085177,
            'owner_id' => User::all()->where('type', 'professional')->random()->id,
            'like' => rand(0, 1),
        ]);

        DB::table('enterprises')->insert([
            'name' => "CLIN ARMELLE / SAUVE BENOIT",
            'description' => "",
            'type' => "grocery",
            'city' => "Toulouse",
            'address' => "31350 Saman",
            'latitude' => 43.238676,
            'longitude' => 0.722949,
            'owner_id' => User::all()->where('type', 'professional')->random()->id,
            'like' => rand(0, 1),
        ]);

        DB::table('enterprises')->insert([
            'name' => "LE PETIT MARCHÉ",
            'description' => "",
            'type' => "grocery",
            'city' => "Gaillac-toulza",
            'address' => "3 Rue Gabriel Fauré, 31550 Gaillac-toulza",
            'latitude' => 43.25464,
            'longitude' => 1.471073,
            'owner_id' => User::all()->where('type', 'professional')->random()->id,
            'like' => rand(0, 1),
        ]);

        DB::table('enterprises')->insert([
            'name' => "LES FROMAGERS DU MONT ROYAL",
            'description' => "",
            'type' => "grocery",
            'city' => "Montréjeau",
            'address' => "31210 Montréjeau",
            'latitude' => 43.081228,
            'longitude' => 0.567879,
            'owner_id' => User::all()->where('type', 'professional')->random()->id,
            'like' => rand(0, 1),
        ]);

        DB::table('enterprises')->insert([
            'name' => "LA BOUSSOLE",
            'description' => "",
            'type' => "grocery",
            'city' => "Barbazan",
            'address' => "Avenue Du Lac, 31510 Barbazan",
            'latitude' => 43.035456,
            'longitude' => 0.615666,
            'owner_id' => User::all()->where('type', 'professional')->random()->id,
            'like' => rand(0, 1),
        ]);

        DB::table('enterprises')->insert([
            'name' => "ÉPICERIE À LA RESCOUSSE",
            'description' => "",
            'type' => "grocery",
            'city' => "Franquevielle",
            'address' => "9 Rue Du 15 Aout 31210 Franquevielle",
            'latitude' => 43.136564,
            'longitude' => 0.534278,
            'owner_id' => User::all()->where('type', 'professional')->random()->id,
            'like' => rand(0, 1),
        ]);

        DB::table('enterprises')->insert([
            'name' => "LA PETITE LIÉGEOISE",
            'description' => "",
            'type' => "grocery",
            'city' => "Bagnères-de-luchon",
            'address' => "21 Avenue Alexandre Dumas 31110 Bagnères-de-luchon",
            'latitude' => 42.790834,
            'longitude' => 0.594762,
            'owner_id' => User::all()->where('type', 'professional')->random()->id,
            'like' => rand(0, 1),
        ]);

        DB::table('enterprises')->insert([
            'name' => "CAVE SYLVAIN BARTHE",
            'description' => "",
            'type' => "grocery",
            'city' => "Bagnères-de-luchon",
            'address' => "3 Rue Julien Sacaze 31110 Bagnères-de-luchon",
            'latitude' => 42.791742,
            'longitude' => 0.591652,
            'owner_id' => User::all()->where('type', 'professional')->random()->id,
            'like' => rand(0, 1),
        ]);

        DB::table('enterprises')->insert([
            'name' => "MAISON PENE",
            'description' => "",
            'type' => "grocery",
            'city' => "Bagnères-de-luchon",
            'address' => "12 Avenue Carnot 31110 Bagnères-de-luchon",
            'latitude' => 42.790447,
            'longitude' => 0.59142,
            'owner_id' => User::all()->where('type', 'professional')->random()->id,
            'like' => rand(0, 1),
        ]);

        DB::table('enterprises')->insert([
            'name' => "LA GENTILHOMMIÈRE",
            'description' => "",
            'type' => "grocery",
            'city' => "Fos",
            'address' => "Avenue Jean Jaures 31440 Fos",
            'latitude' => 42.87364,
            'longitude' => 0.731625,
            'owner_id' => User::all()->where('type', 'professional')->random()->id,
            'like' => rand(0, 1),
        ]);

        DB::table('enterprises')->insert([
            'name' => "QU'ES AQUO",
            'description' => "",
            'type' => "grocery",
            'city' => "Saleich",
            'address' => "26 Rue De L Estélas, 31260 Saleich",
            'latitude' => 43.025545,
            'longitude' => 0.970934,
            'owner_id' => User::all()->where('type', 'professional')->random()->id,
            'like' => rand(0, 1),
        ]);

        DB::table('enterprises')->insert([
            'name' => "LA RUCHE QUI DIT OUI GARE DE BOUSSENS",
            'description' => "",
            'type' => "grocery",
            'city' => "Boussens",
            'address' => "Avenue De La Gare 31360 Boussens",
            'latitude' => 43.17728,
            'longitude' => 0.971349,
            'owner_id' => User::all()->where('type', 'professional')->random()->id,
            'like' => rand(0, 1),
        ]);

        DB::table('enterprises')->insert([
            'name' => "SARL CHLEA",
            'description' => "",
            'type' => "grocery",
            'city' => "Rieux-volvestre",
            'address' => "7 Place Du Préau 31310 Rieux-volvestre",
            'latitude' => 43.2578,
            'longitude' => 1.19604,
            'owner_id' => User::all()->where('type', 'professional')->random()->id,
            'like' => rand(0, 1),
        ]);

        DB::table('enterprises')->insert([
            'name' => "L'OUSTAL DU JARDINIER-NOÉ",
            'description' => "",
            'type' => "grocery",
            'city' => "Noé",
            'address' => "110 Route De Toulouse 31410 Noé",
            'latitude' => 43.354373,
            'longitude' => 1.274378,
            'owner_id' => User::all()->where('type', 'professional')->random()->id,
            'like' => rand(0, 1),
        ]);

        DB::table('enterprises')->insert([
            'name' => "V'OZ GOURMANDISES NATURELLES",
            'description' => "",
            'type' => "grocery",
            'city' => "Longages",
            'address' => "45 Chemin De Noé, 31410 Longages",
            'latitude' => 43.354843,
            'longitude' => 1.24975,
            'owner_id' => User::all()->where('type', 'professional')->random()->id,
            'like' => rand(0, 1),
        ]);
        
        DB::table('enterprises')->insert([
            'name' => "Le petit élevage des fabres",
            'description' => "Le Petit Elevage des Fabres est né de l’envie de s’orienter vers un métier qui ait du sens, l'envie de produire des volailles de qualité, en privilégiant une production diversifiée plutôt qu’une production en quantité.",
            'type' => "grower",
            'city' => "Vacquiers",
            'like' => rand(0,1),
            'address' => "3995 Route de la Magdeleine",
            'owner_id' => User::all()->where('type','professional')->random()->id,
            'longitude' => 0.078107,
            'latitude' => 46.014586,
            
        ]);
        DB::table('enterprises')->insert([
            'name' => "Midi Cueillette",
            'description' => "Exploitation maraîchère aux portes de Toulouse, en conversion vers l’agriculture biologique, nous sommes ouverts à la libre cueillette.",
            'type' => "grower",
            'city' => "Portet-sur-Garonne",
            'like' => rand(0,1),
            'address' => "Chemin Sables",
            'owner_id' => User::all()->where('type','professional')->random()->id,
            'longitude' => 1.4118182,
            'latitude' => 43.5365123,
            
        ]);
        DB::table('enterprises')->insert([
            'name' => "La Ferme de Cagueloup",
            'description' => "Fidèles à la tradition, nos produits régionaux de première qualité font bon ménage avec les recettes du Sud-Ouest.",
            'type' => "grower",
            'city' => "Montgiscard",
            'like' => rand(0,1),
            'address' => "250 Route de Saint-Léon",
            'owner_id' => User::all()->where('type','professional')->random()->id,
            'longitude' => 1.5736238,
            'latitude' => 43.4350833,
            
        ]);
        DB::table('enterprises')->insert([
            'name' => "Ferme de Soubiane",
            'description' => "L'exploitation située sur la commune de Boussan (Haute-Garonne) s'étend sur environ 30 ha ce qui nous permet d'auto produire la plupart des aliments (céréales, foin, paille, glands de chêne vert...) pour les animaux, le complément provenant de petites exploitations labellisées AB situées dans un rayon de 60km.",
            'type' => "grower",
            'city' => "Boussan",
            'like' => rand(0,1),
            'address' => "Scalete",
            'owner_id' => User::all()->where('type','professional')->random()->id,
            'longitude' => 0.889154,
            'latitude' => 43.249547,
            
        ]);
        DB::table('enterprises')->insert([
            'name' => "Cueillette de Fraises, Fleurs & Asperges",
            'description' => "La cueillette est située à Lavernose-Lacasse (Haute-Garonne 31410), à 30 minutes au sud de Toulouse. Toute proche de l’autoroute A64, la cueillette est très facilement accessible depuis Toulouse.",
            'type' => "grower",
            'city' => "Lavernose-Lacasse",
            'like' => rand(0,1),
            'address' => "Comménian",
            'owner_id' => User::all()->where('type','professional')->random()->id,
            'longitude' => 1.4516224,
            'latitude' => 43.597824,
            
        ]);
        DB::table('enterprises')->insert([
            'name' => "Domaine de la Faugade",
            'description' => "Créé en 1958, l'exploitation familiale située à Merville, près de Toulouse, a développé au fil des temps plusieurs variétés de pommes et de poires qu'elle cultive en lutte raisonnée.",
            'type' => "grower",
            'city' => "Merville",
            'like' => rand(0,1),
            'address' => "4939 Route de Toulouse",
            'owner_id' => User::all()->where('type','professional')->random()->id,
            'longitude' => 1.345057,
            'latitude' => 43.71078,
            
        ]);
        DB::table('enterprises')->insert([
            'name' => "A Travers Champs",
            'description' => "Créé en 1958, l'exploitation familiale située à Merville, près de Toulouse, a développé au fil des temps plusieurs variétés de pommes et de poires qu'elle cultive en lutte raisonnée.",
            'type' => "grower",
            'city' => "Belberaud",
            'like' => rand(0,1),
            'address' => "1 Chemin de Pompertuzat",
            'owner_id' => User::all()->where('type','professional')->random()->id,
            'longitude' => 1.345057,
            'latitude' => 43.71078,
            
        ]);
        DB::table('enterprises')->insert([
            'name' => "La Ferme de Borde Bio",
            'description' => "Créé en 1958, l'exploitation familiale située à Merville, près de Toulouse, a développé au fil des temps plusieurs variétés de pommes et de poires qu'elle cultive en lutte raisonnée.",
            'type' => "grower",
            'city' => "Toulouse",
            'like' => rand(0,1),
            'address' => "79 Chemin des Izards",
            'owner_id' => User::all()->where('type','professional')->random()->id,
            'longitude' => 1.442678,
            'latitude' => 43.649066,
            
        ]);
        DB::table('enterprises')->insert([
            'name' => "Ferme Attitude",
            'description' => "Ferme Attitude est le premier magasin de producteurs fermiers au centre de Toulouse, co-fondé en 2010 par Muriel Porry et 18 agriculteurs de la région, il est dédié à la vente directe.",
            'type' => "grower",
            'city' => "Toulouse",
            'like' => rand(0,1),
            'address' => "4 Rue Villeneuve",
            'owner_id' => User::all()->where('type','professional')->random()->id,
            'longitude' => 1.429549,
            'latitude' => 43.598172,
            
        ]);
        DB::table('enterprises')->insert([
            'name' => "La Nursery du Potager",
            'description' => "La Nursery du Potager produit toute l'année des plants BIO de variétés anciennes légumes (tomates, courgettes, poivrons, salades, poireaux, choux...) et aromatiques (persil, basilic, thym, ...) dans le respect des saisons pour les maraîchers professionnels et les jardiniers amateurs.",
            'type' => "grower",
            'city' => "Belberaud",
            'like' => rand(0,1),
            'address' => "Route de Fourquevaux",
            'owner_id' => User::all()->where('type','professional')->random()->id,
            'longitude' => 1.565743,
            'latitude' => 43.510769,
            
        ]);
        DB::table('enterprises')->insert([
            'name' => "Ferme Bio Cassagne",
            'description' => "Ferme BIO Cassagne. Fabrication de l'alimentation céréales bio de ferme locale",
            'type' => "grower",
            'city' => "Lacaugne",
            'like' => rand(0,1),
            'address' => "Cassagne",
            'owner_id' => User::all()->where('type','professional')->random()->id,
            'longitude' => 1.2913,
            'latitude' => 43.281132,
            
        ]);
        DB::table('enterprises')->insert([
            'name' => "Apijoe",
            'description' => "Apijoé, produits de la ruche, ruchers situés dans les Pyrénées orientales et à la limite de l’Ariège, n°Apiculteur 31000391, deux ruchers (40 ruches environ), expédition uniquement en seau de 4 kg net de miel par colissimo",
            'type' => "grower",
            'city' => "Toulouse",
            'like' => rand(0,1),
            'address' => "14 Rue Vincent Auriol",
            'owner_id' => User::all()->where('type','professional')->random()->id,
            'longitude' => 1.394863,
            'latitude' => 43.583023,
            
        ]);
        DB::table('enterprises')->insert([
            'name' => "Les Producteurs Bio",
            'description' => "Producteur maraicher Bio à Launac (31330) depuis 4 générations, j’ai décidé d’ouvrir un primeur bio en centre ville pour pouvoir proposer notre production ainsi que celle de mes collegues",
            'type' => "grower",
            'city' => "Toulouse",
            'like' => rand(0,1),
            'address' => "115 Avenue Camille Pujol",
            'owner_id' => User::all()->where('type','professional')->random()->id,
            'longitude' => 1.466373,
            'latitude' => 43.600728,
            
        ]);
        DB::table('enterprises')->insert([
            'name' => "La Ferme des Cochons Bio",
            'description' => "Ma ferme de 21 ha est spécialisée dans l'élevage, plein air, de porcs en agriculture biologique . Elle vise à une meilleure harmonie entre l'agriculture et la nature, grâce à des pratiques plus respectueuses de l'environnement et des animaux. Les races de mes cochons sont le Gascon, le Large white -Landrace et le Duroc; l'alimentation de mes animaux est 100% bio. Un accueil famillial vous est réservé pour visiter la ferme au coeur d'un environnement boisé et pour transmettre des recettes traditionnelles et réunionnaises.",
            'type' => "grower",
            'city' => "Bretx",
            'like' => rand(0,1),
            'address' => "La Houero",
            'owner_id' => User::all()->where('type','professional')->random()->id,
            'longitude' => 1.1824801,
            'latitude' => 43.695255,
            
        ]);
        DB::table('enterprises')->insert([
            'name' => "La Ferme du Roc",
            'description' => "La Ferme du Roc, éleveur de canards gras dans le Lauragais à Cambiac depuis 1990, nos canards sont élevés en plein air sur plus de 2 Ha de prairie.",
            'type' => "grower",
            'city' => "Cambiac",
            'like' => rand(0,1),
            'address' => "Le Roc",
            'owner_id' => User::all()->where('type','professional')->random()->id,
            'longitude' => 1.1824801,
            'latitude' => 43.695255,
            
        ]);
        DB::table('enterprises')->insert([
            'name' => "La Ferme du Roc",
            'description' => "La Ferme du Roc, éleveur de canards gras dans le Lauragais à Cambiac depuis 1990, nos canards sont élevés en plein air sur plus de 2 Ha de prairie.",
            'type' => "grower",
            'city' => "Cambiac",
            'like' => rand(0,1),
            'address' => "Le Roc",
            'owner_id' => User::all()->where('type','professional')->random()->id,
            'longitude' => 1.8030346,
            'latitude' => 43.4858154,
            
        ]);
        DB::table('enterprises')->insert([
            'name' => "Ferme de Cantegril",
            'description' => " produits de la ferme : Lait cru, vente à la ferme toute l'année. Confitures de lait, confitures de pêches et de pastèques, guignes. Fruits : Pêches et nectarines en juillet et en août.",
            'type' => "grower",
            'city' => "Villematier",
            'like' => rand(0,1),
            'address' => "1953 Route de Vacquiers",
            'owner_id' => User::all()->where('type','professional')->random()->id,
            'longitude' => 1.5073276,
            'latitude' => 43.8191423,
            
        ]);
        DB::table('enterprises')->insert([
            'name' => "Ferme de Clejust",
            'description' => "Situé sur la commune de Cazères (en Haute-Garonne), le verger s’étend sur 9Ha dont 4 H sont consacrés à la culture de kiwis. Les arbres à kiwis, appelés actinidia, sont cultivés en bio et produisent au total entre 30 et 80 tonnes selon les années… En 2020, les Jardins du Volvestre, basés à Salles s/Garonne, ont racheté le verger.",
            'type' => "grower",
            'city' => "Cazères",
            'like' => rand(0,1),
            'address' => "Chemin de Malaret",
            'owner_id' => User::all()->where('type','professional')->random()->id,
            'longitude' => 1.1026631,
            'latitude' => 43.2188374,
            
        ]);
        DB::table('enterprises')->insert([
            'name' => "La ferme de Montplaisir",
            'description' => "Notre ferme, en polyculture-élevage sous label Agriculture Biologique, située en plein cœur du Lauragais, a été crée en 2017. Nous nous sommes tous deux reconvertis dans le métier de paysans: nous élevons des volailles (poulets, pintades et volailles fêtes) et produisons nos céréales pour être autonomes et assurer la traçabilité de l'alimentation de nos volailles.",
            'type' => "grower",
            'city' => "Falga",
            'like' => rand(0,1),
            'address' => "Montplaisir",
            'owner_id' => User::all()->where('type','professional')->random()->id,
            'longitude' => 1.8450166,
            'latitude' => 43.4783472,
            
        ]);
        DB::table('enterprises')->insert([
            'name' => "La cueillette des fruits rouges",
            'description' => "Notre ferme, en polyculture-élevage sous label Agriculture Biologique, située en plein cœur du Lauragais, a été crée en 2017. Nous nous sommes tous deux reconvertis dans le métier de paysans: nous élevons des volailles (poulets, pintades et volailles fêtes) et produisons nos céréales pour être autonomes et assurer la traçabilité de l'alimentation de nos volailles.",
            'type' => "grower",
            'city' => "Labège",
            'like' => rand(0,1),
            'address' => "1812 Route de Baziege la Lauragaise",
            'owner_id' => User::all()->where('type','professional')->random()->id,
            'longitude' => 1.5247374,
            'latitude' => 43.5394538,
            
        ]);
    }
}
