<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Enterprise;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class EnterpriseController extends Controller
{
    /**
     * Create the controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(Enterprise::class, 'enterprise');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Enterprise::all();
    }

    /**
     * Search a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function resultsFilters($request, $enterprise)
    {
        if ($request->city!=null){
            $item = [];
            foreach ($enterprise as $key => $value) {
                if(in_array($value['city'], $request->city)){
                    array_push($item, $value);
                }
            }
            $enterprise = $item;
        }
        if ($request->product!=null){
            $item = [];
            foreach ($enterprise as $key => $value) {
                foreach ($value['product_categories'] as $prod) {
                    if(in_array($prod['name'], $request->product)){
                        array_push($item, $value);
                    }
                }
            }
            $enterprise = $item;
        }
        if ($request->rate!=null){
            $item = [];
            foreach ($enterprise as $key => $value) {
                if(in_array((int)($value['rate']), $request->rate)){
                    array_push($item, $value);
                }
            }
        }
        return $item;
    }

    /**
     * Search a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $per_page = (int)$request->query("per_page", 15);
        if($request->type!=null){
            if($request->name!=null){
                $enterprise = Enterprise::where('type', $request->type)->where('name','ilike', '%'.$request->name.'%')->orderBy("id")->paginate($per_page);
                return $enterprise;
            }
            if($request->city==null && $request->product==null && $request->rate==null){
                $enterprise = Enterprise::where('type', $request->type)->orderBy("id")->paginate($per_page);
                return $enterprise;
            }else{
                $enterprise = Enterprise::where('type', $request->type)->get();
                $currentpage = \Request::get('page', 1);
                $offset = ($currentpage * $per_page) - $per_page ;
            }
            $item = $this->resultsFilters($request, $enterprise);
            $itemstoshow = array_slice($item , $offset , $per_page);
            $enterprise = new LengthAwarePaginator($itemstoshow,count($item),$per_page,null);
        }else{
            $enterprise = Enterprise::orderBy("id")->paginate($per_page);
        }
        return $enterprise;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function filters(Request $request)
    {
        $countCity = [];
        $enterprises = Enterprise::where('type', $request->type)->get();
        foreach ($enterprises as $value) {
            if(!array_key_exists($value['city'], $countCity)){
                $countCity[$value['city']] = 0;
                }
            $countCity[$value['city']] = $countCity[$value['city']] + 1;
        }
        $countRate = [];
        for ($i = 0; $i <= 5; $i++) {
            $countRate[$i] = 0;
        }
        foreach ($enterprises as $value) {
            $countRate[$value['rate']] = $countRate[$value['rate']] + 1;
        }
        $countProduct = [];
        foreach ($enterprises as $value) {
            foreach ($value['product_categories'] as $product) {
                if(!array_key_exists($product->name, $countProduct)){
                    $countProduct[$product->name] = 0;
                }
                $countProduct[$product->name] = $countProduct[$product->name] + 1;
            }
        }
        $filter['cities'] = $countCity;
        $filter['products'] = $countProduct;
        $filter['rates'] = $countRate;
        return $filter;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Enterprise::create($request->all());
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Enterprise  $enterprise
     * @return \Illuminate\Http\Response
     */
    public function show(Enterprise $enterprise)
    {
        return $enterprise;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Enterprise  $enterprise
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Enterprise $enterprise)
    {   
        $data = $request->except("like");
        if($request->hasFile("picture")) {
            $data["picture"] = $request->file("picture")
                                        ->storeAs("enterprises", $enterprise->id);
        }

        return $enterprise->update(($request->user()->isExpert()) ?
                                        $request->only("like") :
                                        $data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Enterprise  $enterprise
     * @return \Illuminate\Http\Response
     */
    public function destroy(Enterprise $enterprise)
    {
        return $enterprise->delete();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Enterprise  $enterprise
     * @return \Illuminate\Http\Response
     */
    public function getEnterprisesByBounds(Request $request)
    {
        return collect(DB::select('SELECT id, latitude, longitude, type, name FROM enterprises WHERE latitude >= ? AND latitude <= ? AND longitude >= ? AND longitude <= ?', 
        [$request->_southWest["lat"], $request->_northEast["lat"], $request->_southWest["lng"], $request->_northEast["lng"]]))->map(function ($enterprise) {
            $enterprise->coordinates = [(float)$enterprise->latitude, (float)$enterprise->longitude];
            return $enterprise;
        });

        /* return Enterprise::where('latitude', '>=', $request->_southWest["lat"])
            ->where('latitude', '<=', $request->_northEast["lat"])
            ->where('longitude', '>=', $request->_southWest["lng"])
            ->where('longitude', '<=', $request->_northEast["lng"])
            ->get()->only(["id", "latitude", "longitude", "name"]); */
    }
}
