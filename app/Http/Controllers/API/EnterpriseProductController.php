<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Enterprise;
use App\Models\EnterpriseProduct;
use Illuminate\Http\Request;

class EnterpriseProductController extends Controller
{
    /**
     * Create the controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->authorizeResource(EnterpriseProduct::class, 'enterprise_product');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \App\Models\Enterprise  $enterprise
     * @return \Illuminate\Http\Response
     */
    public function index(Enterprise $enterprise)
    {
        return $enterprise->products()->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Models\Enterprise  $enterprise
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Enterprise $enterprise, Request $request)
    {
        if(!$request->user()->isAdministrator() && $request->user() != $enterprise->owner()->first()) {
            return abort(403);
        }

        return $enterprise->products()->attach($request->input("id"));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Enterprise  $enterprise
     * @param  \App\Models\EnterpriseProduct  $enterpriseProduct
     * @return \Illuminate\Http\Response
     */
    public function show(Enterprise $enterprise, EnterpriseProduct $enterpriseProduct)
    {
        return $enterpriseProduct;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\Enterprise  $enterprise
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EnterpriseProduct  $enterpriseProduct
     * @return \Illuminate\Http\Response
     */
    public function update(Enterprise $enterprise, Request $request, EnterpriseProduct $enterpriseProduct)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Enterprise  $enterprise
     * @param  \App\Models\EnterpriseProduct  $enterpriseProduct
     * @return \Illuminate\Http\Response
     */
    public function destroy(Enterprise $enterprise, Request $request, int $product)
    {
        if(!$request->user()->isAdministrator() && $request->user() != $enterprise->owner) {
            return abort(403);
        }

        return $enterprise->products()->where("product_id", $product)->delete();
    }
}
