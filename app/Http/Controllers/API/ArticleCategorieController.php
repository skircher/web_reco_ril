<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\ArticleCategorie;
use App\Models\Article;
use App\Models\Categorie;
use Illuminate\Http\Request;

class ArticleCategorieController extends Controller
{
    /**
     * Create the controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(ArticleCategorie::class, 'article_category');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index(Article $article)
    // {
    //     return $article->categories()->get();
    // }
    public function index(Article $article)
    {
        //return ArticleCategorie::all();
        return $article->categories()->get();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listBycategorie(Categorie $categorie, Request $request)
    {
        $per_page = (int)$request->query("per_page", 10);
        $articles = $categorie->articles()->orderBy('id')->paginate($per_page);
        
        return $articles;
        //return $articles;
    }
        /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Article $article, Request $request)
    {
        //return ArticleCategorie::create($request->all());
        return $article->categories()->attach($request->input("categorie_id"));

    }
}