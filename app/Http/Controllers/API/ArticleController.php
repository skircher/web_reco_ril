<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\ArticleCategorie;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    /**
     * Create the controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(Article::class, 'article');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Article::all();
    }

    /**
     * Search a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $per_page = (int)$request->query("per_page", 10);

        $articles = Article::orderBy("date", "desc");
        if($request->category) {
            $articles->whereHas("categories", function($query) use ($request) {
                $query->where("categorie_id", $request->category);
            });
        }

        return $articles->paginate($per_page);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $article = Article::create($request->all());

        $article->categories()->attach(collect($request->categories)->map(function($category) {
            return $category["id"];
        }));

        return $article;
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        return $article;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
        $categories = collect($request->categories)->map(function($category) {
            return $category["id"];
        });
        $article->categories()->whereNotIn("id", $categories)->detach();

        $current_categories = $article->categories()->get()->map(function($category) {
            return $category->id;
        });
        $new_categories = collect($categories)->whereNotIn("id", $current_categories);
        $article->categories()->attach($new_categories);

        return $article;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $article->delete();
    }
}
