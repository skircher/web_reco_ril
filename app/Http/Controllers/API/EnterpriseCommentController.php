<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Mail\EnterpriseCommented;
use App\Models\Enterprise;
use App\Models\EnterpriseComment;
use App\Models\EnterpriseReviewType;
use App\Models\EnterpriseUserReview;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use App\Http\Resources\EnterpriseComment as EnterpriseCommentResource;

class EnterpriseCommentController extends Controller
{
    /**
     * Create the controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(EnterpriseComment::class, 'comment');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \App\Models\Enterprise  $enterprise
     * @return \Illuminate\Http\Response
     */
    public function index(Enterprise $enterprise)
    {
        return EnterpriseComment::where("enterprise_id", $enterprise->id)->get();
    }

    /**
     * Search a listing of the resource.
     *
     * @param  \App\Models\Enterprise  $enterprise
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Enterprise $enterprise, Request $request)
    {
        $per_page = (int)$request->query("per_page", 15);
        $user_type = $request->query("user_type", null);

        $comments = EnterpriseComment::where("enterprise_id", $enterprise->id);
        if($user_type) {
            $comments->whereHas("user", function($query) use ($user_type) {
                $query->where("type", $user_type);
            });
        }

        return $comments->orderBy("id")->paginate($per_page);
    }

    /**
     * Check if user has already comment
     * 
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Http\Request  $request
     * @return Boolean
     */
    public function has_comment(Enterprise $enterprise, Request $request) {
        return response()->json($request->user()
                                    ->comments()
                                    ->where("enterprise_id", $enterprise->id)->exists());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Models\Enterprise  $enterprise
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Enterprise $enterprise, Request $request)
    {
        $comment = new EnterpriseComment();
        $comment->user()->associate($request->user());
        $comment->message = $request->message;

        $enterprise->reviews()->saveMany(collect($request->reviews)->filter(function($review) use($enterprise, $request) {
            return !EnterpriseUserReview::where("enterprise_id", $enterprise->id)
                                        ->where("user_id", $request->user()->id)
                                        ->where("type_id", $review["id"])
                                        ->exists();
        })->map(function($review) use ($request) {
            $user_review = new EnterpriseUserReview();

            $user_review->user()->associate($request->user());
            $user_review->type_id = $review["id"];
            $user_review->review = $review["score"];

            return $user_review;
        })->all());

        Mail::to($enterprise->owner)
                ->send(new EnterpriseCommented($comment));

        return $enterprise->comments()->save($comment);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Enterprise  $enterprise
     * @param  \App\Models\EnterpriseComment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Enterprise $enterprise, EnterpriseComment $comment)
    {
        return $comment;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\Enterprise  $enterprise
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EnterpriseComment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Enterprise $enterprise, Request $request, EnterpriseComment $comment)
    {
        if($request->user()->isAdministrator()) {
            return $comment->update($request->all());
        } else {
            return $comment->update($request->only("message"));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Enterprise  $enterprise
     * @param  \App\Models\EnterpriseComment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Enterprise $enterprise, EnterpriseComment $comment)
    {
        return $comment->delete();
    }
}
