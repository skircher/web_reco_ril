<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\EnterpriseReviewType;
use Illuminate\Http\Request;

class EnterpriseReviewTypeController extends Controller
{
    /**
     * Create the controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(EnterpriseReviewType::class, 'enterprise_review_type');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return EnterpriseReviewType::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return EnterpriseReviewType::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EnterpriseReviewType  $enterpriseReviewType
     * @return \Illuminate\Http\Response
     */
    public function show(EnterpriseReviewType $enterpriseReviewType)
    {
        dd($enterpriseReviewType);
        return $enterpriseReviewType;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EnterpriseReviewType  $enterpriseReviewType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EnterpriseReviewType $enterpriseReviewType)
    {
        return $enterpriseReviewType->update($request->all);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EnterpriseReviewType  $enterpriseReviewType
     * @return \Illuminate\Http\Response
     */
    public function destroy(EnterpriseReviewType $enterpriseReviewType)
    {
        return $enterpriseReviewType->delete();
    }
}
