<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'img_path', 'date', 'description','alt', 'link'
    ];

        /**
     * The relationships that should always be loaded.
     *
     * @var array
     */

         /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['categories'];

    public function categories() {
        return $this->belongsToMany(Categorie::class, "article_categories");
    }
}
