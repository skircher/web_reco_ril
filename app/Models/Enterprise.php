<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Enterprise extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'type', 'city', 'like', "address", "latitude", "longitude", "owner_id"
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['owner'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['reviews', "product_categories", "rate", "rates", "coordinates"];

    public function owner() {
        return $this->belongsTo(User::class, "owner_id");
    }

    public function products() {
        return $this->belongsToMany(Product::class, "enterprise_products");
    }

    public function productCategories() {
        return $this->products->map(function($value) {
            return ProductCategory::find($value["product_category_id"]);
        });
    }

    public function getReviewsAttribute() {
        return $this->comments()->count();
    }

    public function getProductCategoriesAttribute() {
        $products = $this->products;

        return $this->productCategories()
                    ->map(function($category) use ($products) {
                            $category["products"] = $products->filter(function($product) use ($category) {
                                return $product["product_category_id"] == $category["id"];
                            })->makeHidden(["product_category_id", "pivot"]);

            return $category;
        });
    }

    public function comments() {
        return $this->hasMany(EnterpriseComment::class);
    }

    public function reviews() {
        return $this->hasMany(EnterpriseUserReview::class);
    }

    public function getRateAttribute() {
        return (float)$this->reviews()->avg("review");
    }

    public function getRatesAttribute() {
        return EnterpriseReviewType::all()
                ->map(function($review_type) {
                    $review_type["score"] = (float) EnterpriseUserReview::where("enterprise_id", $this->id)
                                                ->where("type_id", $review_type->id)
                                                ->avg("review");

                    return $review_type;
                });
    }

    public function getCoordinatesAttribute() {
        return [(float)$this->latitude, (float)$this->longitude];
    }
}
