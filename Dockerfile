FROM php:8-fpm-alpine
RUN apk add postgresql-libs postgresql-dev && docker-php-ext-install pdo pdo_pgsql && apk del postgresql-dev

RUN apk add composer

WORKDIR /src

ADD resources ./resources
ADD package.json .
ADD webpack.mix.js .

RUN apk add npm && npm install --silent && npm run --silent dev && rm -rf ./node_modules && npm cache clean --force && rm -rf ~/.npm && apk del npm

ADD storage ./storage
ADD bootstrap ./bootstrap

ADD artisan .
ADD server.php .

ADD app ./app
ADD config ./config
ADD database ./database
ADD routes ./routes

RUN mkdir bootstrap/cache

ADD composer.json .
RUN composer install --optimize-autoloader --no-dev && composer clear-cache && rm -rf ~/.composer

ADD public/favicon.ico ./public/favicon.ico
ADD public/index.php ./public/index.php
ADD public/mix-manifest.json ./public/mix-manifest.json
ADD public/robots.txt ./public/robots.txt

RUN echo -e "upload_max_filesize = 64M\npost_max_size = 64M" > /usr/local/etc/php/conf.d/uploads.ini

ADD entrypoint.sh /

ENTRYPOINT [ "/entrypoint.sh" ]
CMD ["php", "artisan", "serve", "--host", "0.0.0.0"]
